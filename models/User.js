const mongoose = require('mongoose')
// const Schema = mongoose.Schema
const { Schema } = mongoose    //Line 2 and 3 are same....line 3 is basically ES2015 syntax


const userSchema = new Schema ({
  googleId: String,
  credits:{
    type:Number,
    default:0
  }
})

mongoose.model('user',userSchema)