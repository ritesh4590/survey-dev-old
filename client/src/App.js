import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom'

import './App.css';
import { connect } from 'react-redux'

import Header from './components/Header'
import { fetchUser } from './actions/index'

const DashBoard = () => <h2>Dashboard</h2>
const Landing = () => <h2>Landing</h2>
const Survey = () => <h2>Survey</h2>

class App extends React.Component {

  componentDidMount() {
    this.props.fetchUser()
  }

  render() {
    return (
      <div className="container">
        <BrowserRouter>
          <div>
            <Header />
            <Route path="/" component={Landing} exact />
            <Route path="/dashboard" component={DashBoard} />
            <Route path="/survey" component={Survey} />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default connect(null, {fetchUser})(App);
