import React, { Component } from 'react'
import StripeCheckout from 'react-stripe-checkout';
import { connect } from 'react-redux'
import { handleToken } from '../actions/index'
class Payments extends Component {
	render() {
		// debugger
		return (
			<div>
				<StripeCheckout
					name="Survey"
					description="5$ from 5 Survey"
					amount={500}
					token={token => this.props.handleToken(token)}
					stripeKey={process.env.REACT_APP_STRIPE_KEY}
				>
					<button className="btn">
						ADD CREDITS
				</button>
				</StripeCheckout>
			</div>
		)
	}
}

export default connect(null, { handleToken })(Payments)
