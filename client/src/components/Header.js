import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Payments from './Payments'

import '../App.css'

class Header extends Component {

  handleText = () => {
    if (this.props.auth === null) {
      return 'Loading'
    }
    else {
      return !this.props.auth ?
        <li><a href="/auth/google">Login With Google</a></li>
        :
        <>
          <li><Payments /></li>
          <li style={{margin:'0 10px'}}>Credits:{this.props.auth.credits}</li>
          <li><a href="/api/logout">Logout</a></li>
        </>
    }
  }

  render() {
    return (
      <nav>
        <div className="nav-wrapper">
          <Link to={this.props.auth ? '/dashboard' : '/survey'} className="left brand-logo" style={{ marginLeft: '10px' }}>Surveys</Link>

          <ul className="right">
            {this.handleText()}
          </ul>
        </div>

      </nav>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps)(Header)
